# Requirements: https://pypi.org/project/websocket-client/
from websocket import create_connection
import json
import random
import time
import sys

# URL = "ws://localhost:3012"
URL = "ws://dmc.alepoydes.com:3012"

def receive():
    result = json.loads(ws.recv())
    print("in:", result)
    if result["state"]=="error":
        print("Error: {}".format(result["error"]))
        return None
    return result

def send(msg):
    print("out:", msg)
    ws.send(json.dumps(msg))

if __name__ == "__main__":
    random.seed()
    print("Creating connection...")
    ws = create_connection(URL)
    result = receive()
    assert(result["state"]=="info")
    print("Playing \"{}\", version {}".format(result["name"], result["version"]))

    send({"state":"login"})
    result = receive()
    if result is None:
        print("Access denied")
        exit(1)
    assert(result["state"]=="access")
    print("Logged as {}".format(result["user"]))
    sys.stdout.flush()

    while True:
        sys.stdout.flush()
        result = receive()
        if result is None:
            print("no recieve :c")
            time.sleep(0.01)
            continue
        game = result["game"]
        if result["state"]=="start":
            print("New game {} as hand {}".format(game, result["hand"]))
            print("Game parameters: {}".format(result["parameters"]))
        elif result["state"]=="gameover":
            print("Game {} is finished with scores {}".format(game,result["scores"]))
            continue
        elif result["state"]=="turnover":
            print("Game {} end of turn, players moves are {}".format(game, result["moves"]))
            pass
        else: 
            print("Unknown message: {}".format(result))
            exit(1)
        move = random.choice([0, 1])
        send({"state":"move", "strategy": move, "game": game})