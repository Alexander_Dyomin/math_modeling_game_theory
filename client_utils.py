import json

def receive(ws):
    result = json.loads(ws.recv())
    print("in:", result)
    if result["state"]=="error":
        print("Error: {}".format(result["error"]))
        return None
    return result

def send(ws, msg):
    print("out:", msg)
    ws.send(json.dumps(msg))