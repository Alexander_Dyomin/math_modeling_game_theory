from game import Game
import numpy as np
import sys
import random

class CoopGame(Game):

    def __init__(self, hand, terminationProbability, payoff):
        super().__init__(hand, terminationProbability, payoff)
        
    def MakeMove(self):
        if not self.turnsArr:
            return 0 # not risking here
            
        countOfPayments = np.sum(self.turnsArr[-1])   
        if self.turnsArr[-1][self.hand] == 0: # if i didn't pay
            paymentWithoutMe = self.payoff[countOfPayments]
            paymentWithMe = self.payoff[countOfPayments + 1] - 1       
        else:
            paymentWithoutMe = self.payoff[countOfPayments - 1]
            paymentWithMe = self.payoff[countOfPayments] - 1
            
        return 1 if paymentWithMe >= paymentWithoutMe else 0 
            
        # if (self.payoff[1] - 1 >= 0):
            # return 1
        
        # return 0
        
    def AppendTurnResult(self, turn):
        super().AppendTurnResult(turn)
        if(len(self.turnsArr) > 20):
            self.turnsArr = self.turnsArr[1:]
        print("turnsArr = ", self.turnsArr)