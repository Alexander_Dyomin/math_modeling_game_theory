from player import Player
from coop_game import CoopGame
from client_utils import receive, send
import sys

class CoopPlayer(Player):
	
    def CreateGame(self, hand, terminationProbability, payoff):
        return CoopGame(hand, terminationProbability, payoff)
        
    def Proceed(self):
        result = receive(self.ws)
        if result is None:
            print("no recieve :c")
            return
        game = result["game"]
        if result["state"]=="start":
            print("New game {} as hand {}".format(game, result["hand"]))
            print("Game parameters: {}".format(result["parameters"]))
            parameters = result["parameters"]
            sys.stdout.flush()
            self.AppendGame(game, self.CreateGame(result["hand"], parameters["termination_probability"], parameters["income"]))
            sys.stdout.flush()
        elif result["state"]=="gameover":
            print("Game {} is finished with scores {}".format(game,result["scores"]))
            self.RemoveGame(game)
            return
        elif result["state"]=="turnover":
            print("Game {} end of turn, players moves are {}".format(game, result["moves"]))
            self.AppendTurnResult(game, result["moves"])
            sys.stdout.flush()
            pass
        else: 
            return
        move = self.MakeMove(game)
        send(self.ws, {"state":"move", "strategy": move, "game": game})