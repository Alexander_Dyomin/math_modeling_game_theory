from game import Game
import numpy as np
import sys
import random

class CowardGame(Game):
    
    def __init__(self, hand, terminationProbability, payoff):
        super().__init__(hand, terminationProbability, payoff)
        self.isRandom = False # if enemy's playing random, he'll destroy both of us
        self.baseCowardPercent = 0.1
        self.isCoward = False # is we're playing coward strategy
        self.isWorthRisk = False # is risk is worth it (see below)
        
        # I noticed a big problem: high probabilites of game stop. In fact, there's rarely situations
        # where returning 1 is worth it. But I'll try. Because the path of samurai is the path of death!
        
        # NOTE: too many agressive players, that strategy is useless. For now I'm chicken. Uncomment code below to get more agressiveness
        
        gameLength = int(1 / self.terminationProbability) # average game length
        self.crushResult = payoff[1][1][0]
        self.retreatResult = payoff[0][1][0]
        # diff = self.crushResult - self.retreatResult
        # turnsCount = int(diff / self.retreatResult) + 1 # how many turns to compensate one (probably, first) crush
        # if turnsCount <= gameLength:
            # self.isWorthRisk = True
        
    def MakeMove(self):
        if not self.isWorthRisk:
            return self.AttackWhenWorth()
    
        if not self.turnsArr:
            return 1 # crush them down!
            
        if self.isRandom:
            return 0
            
        if self.isCoward:
            return self.AttackWhenWorth()
        
        if len(self.turnsArr) > 1:
            lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
            lastSum = np.sum(lastTurns[-2])
            if lastSum == 0:
                return 1 # can attack now easily
        
        if len(self.turnsArr) > 10:
            self.isRandom = self.IsRandom()
            if self.isRandom:
                return 0
            
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr) # how many dangerous moves opponent made
        sum = np.sum(lastTurns) / len(self.turnsArr)
        if len(self.turnsArr) < 4 and len(self.turnsArr) > 1 and sum == 1.0:
            sum = sum * self.baseCowardPercent
        else:
            return 1 # never retreat, never surrender!
            
            
        if (random.uniform(0, 1) <= sum):
            if not len(self.turnsArr) < 5:
                self.isCoward = True
            return 0
        return 1
               
        return self.turnsArr[-1][self.hand]
        
    def AppendTurnResult(self, turn):
        # self.currentTurn = self.currentTurn + 1
        # self.turnsArr.append(turn)
        super().AppendTurnResult(turn)
        if(len(self.turnsArr) > 20):
            self.turnsArr = self.turnsArr[1:]
        print("turnsArr = ", self.turnsArr)
            # self.turnsArr.pop[0]
            
    def IsEyeForEye(self):
        i = 1
        while i < len(self.turnsArr):
            if (self.turnsArr[i - 1][self.hand] != self.turnsArr[i][1 - self.hand]):
                return False
            i = i + 1
        return True        
    
    def IsUnknownTactic(self): # if enemy making some moves with rare probability, maybe he's playing some tactic and not random
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
        sum = np.sum(lastTurns)
        res = sum / len(self.turnsArr)
        if (res < 0.2 or res > 0.8):
            return True
        return False
    
    def IsAlwaysAgressive(self):
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
        sum = np.sum(lastTurns)
        if (sum == 0):
            return True
        return False
        
    def IsAlwaysCoward(self):
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
        sum = np.sum(lastTurns)
        if (sum == len(self.turnsArr)):
            return True
        return False
        
    def IsRandom(self):
        if (not self.IsAlwaysAgressive() and not self.IsAlwaysCoward() and not self.IsEyeForEye() and not self.IsUnknownTactic()):
            return True
        return False
        
    def AttackWhenWorth(self):
        if len(self.turnsArr) < 4:
            return 0 # need more statistics
        lastTurns = np.sum(list(x[1 - self.hand] for x in self.turnsArr))
        crushProb = lastTurns / len(self.turnsArr)
        crushPayoff = crushProb * self.crushResult
        respectPayoff = -self.retreatResult * (1 - crushProb)
        borderValue = self.retreatResult if self.turnsArr[-1][1 - self.hand] == 1 else 0
        return 1 if (crushPayoff + respectPayoff) >= borderValue else 0
        