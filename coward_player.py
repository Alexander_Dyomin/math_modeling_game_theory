from player import Player
from coward_game import CowardGame

class CowardPlayer(Player):
	
	def CreateGame(self, hand, terminationProbability, payoff):
		return CowardGame(hand, terminationProbability, payoff)