from game import Game
import numpy as np
import sys

class PrisonersDilemmaGame(Game):
    
    def __init__(self, hand, terminationProbability, payoff):
        super().__init__(hand, terminationProbability, payoff)
        # here i wanna calculate that game will end exactly at i turn
        continueProb = 1 - self.terminationProbability
        self.termProbArray = np.zeros(int(2 * 1 / self.terminationProbability))
        tpArrLength = self.termProbArray.size
        # print("size = ", self.termProbArray.size)
        # sys.stdout.flush()
        self.termProbArray[0] = 1
        self.termProbArray[1] = continueProb
        i = 2
        # firstly lets calculate probability that game will continue at i turn
        while(self.termProbArray[i - 1] > 0.00001 and i < tpArrLength):
            self.termProbArray[i] = self.termProbArray[i - 1] * continueProb
            i = i + 1
        # and then probability that i turn would be the last one
        self.termProbArray = self.termProbArray * self.terminationProbability
        
        self.TwoCoopSum = self.payoff[1][1][self.hand] * 2
        self.DiffTurnsSum = self.payoff[self.hand][1 - self.hand][1 - self.hand] + self.payoff[self.hand][1 - self.hand][self.hand]
        
        self.isRandom = False # flag for case enemy player plaing just pure random (cause random can beat eye-for-eye)
    
    # eye for an eye strategy
    def MakeMove(self):
        if not self.turnsArr:
            return 1 # do not betray
            
        if (self.isRandom == True):
            return 0 # for random better always betray
            
        if (self.currentTurn > 5 and self.IsRandom()):
            self.isRandom = True
            return 0 # for random better always betray
        
        # case betrayal and taking damage in sum better than 2 cooperations
        # NOTE : ttemporary disable due to coop bot didn't play eye for eye and I want more scores.
        # if(self.currentTurn > 2): # making sure we're cooperating with another bot
            # if(self.DiffTurnsSum > self.TwoCoopSum):
                # lastTurns = (list(x[1 - self.hand] for x in self.turnsArr[-3:]))
                # lastTurnsSum = np.sum(lastTurns)
                # # before this we either played coop or go for betrayal because of its effectivness
                # # so enemy must have sum of 3 if he didn't betrayed us, or our betrayal wouldn't be effective
                # if(lastTurnsSum == 3):
                    # return 0
        
        # after that our bot must play as usual eye for an eye and effective turns of 0-1-0-1 would be effective for both players
        #if enemy didn't play eye for an eye.. well, ok, bad for him
        
        if(self.termProbArray.size > self.currentTurn):
            # lets try find out when to betray
            if(self.turnsArr[len(self.turnsArr) - 1][1 - self.hand] == 0):
                return 0 # because fu... we can't do any betrayal here
            # calculating how much we can get from betrayal
            
            betrayalSum = np.sum(self.termProbArray[(self.currentTurn + 1):]) * self.payoff[0][0][self.hand]
            betrayalSum = betrayalSum + self.termProbArray[self.currentTurn] * self.payoff[self.hand][1 - self.hand][self.hand]
            # and no betrayal
            goodSum = np.sum(self.termProbArray[self.currentTurn:]) * self.payoff[1][1][self.hand]
            if(betrayalSum > goodSum):
                return 0
        return self.turnsArr[len(self.turnsArr) - 1][1 - self.hand]
    
    def AppendTurnResult(self, turn):
        # self.currentTurn = self.currentTurn + 1
        # self.turnsArr.append(turn)
        super().AppendTurnResult(turn)
        if(len(self.turnsArr) > 10):
            self.turnsArr = self.turnsArr[1:]
        print("turnsArr = ", self.turnsArr)
            # self.turnsArr.pop[0]
            
    def IsEyeForEye(self):
        print("in IsEyeForEye")
        sys.stdout.flush()
        i = 1
        while i < len(self.turnsArr):
            if (self.turnsArr[i - 1][self.hand] != self.turnsArr[i][1 - self.hand]):
                print("IsEyeForEye end with False")
                sys.stdout.flush()
                return False
            i = i + 1
        print("IsEyeForEye end with True")
        sys.stdout.flush()
        return True
        
    def IsAlwaysBetray(self):
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
        sum = np.sum(lastTurns)
        if (sum == 0):
            return True
        return False
        
    def IsAlwaysCoop(self):
        lastTurns = list(x[1 - self.hand] for x in self.turnsArr)
        sum = np.sum(lastTurns)
        if (sum == len(self.turnsArr)):
            return True
        return False
        
    def IsRandom(self):
        if (not self.IsAlwaysBetray() and not self.IsAlwaysCoop() and not self.IsEyeForEye()):
            return True
        return False