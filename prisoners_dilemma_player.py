from player import Player
from prisoners_dilemma_game import PrisonersDilemmaGame

class PrisonersDilemmaPlayer(Player):
	
	def CreateGame(self, hand, terminationProbability, payoff):
		return PrisonersDilemmaGame(hand, terminationProbability, payoff)